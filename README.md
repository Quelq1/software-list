# Liste de logiciel

## Carte mentale
[draw.io](https://www.draw.io/)

## Logiciel de présentations
[sozi](http://sozi.baierouge.fr/pages/10-about-fr.html)
[bespoke.js](https://github.com/bespokejs/bespoke)
[impress.js ♥](https://impress.js.org/)
[reveal.js ♥](https://revealjs.com/#/)
[slides.com (Editeur reveal)](slides.com)
[prezi](prezi.com)


## Editeur

### Carte mentale
[Marp](https://yhatt.github.io/marp/)
[DBDiagram.io](DBDiagram.io G)

## Editeur d'algorithme
[Dia (dessin d'algorithme)](https://wiki.gnome.org/Apps/Dia)


## Createur d'interface
[Pencil Project](https://pencil.evolus.vn/)

